# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

Author: Brenna Stutenroth
email: bvs@uoregon.edu

Reads credentials.ini and prints the message we wrote which, in this case, was Hello World
